import { cardsApi } from "../api/cards.js";

const createCardFunc = async (cardData) => {
    try {
        const newCard = await cardsApi.createCard(cardData);

        return newCard
    } catch (error) {
        console.error(error.message);
    }
}

export default createCardFunc