
import { doctorTypeSelect, addUniqueFieldsModal } from "./addUniqueFieldsModal.js";

const renderModals = () => {
  const modalsArr = document.querySelectorAll(".modal");

  modalsArr.forEach((modal) => {
    const activatorId = modal.getAttribute("data-modal-activator");
    const modalActivator = document.getElementById(activatorId);

    modalActivator.addEventListener("click", () => {
      modal.classList.toggle("active");
    });

    const closeBtn = modal.querySelector(".modal__close");

    closeBtn.addEventListener("click", () => {
      handleModalClose(modal);
    });

    const modalBody = modal.querySelector(".js-modal-body");

    modalBody.addEventListener("click", (e) => {
      e.stopImmediatePropagation();
      e.stopPropagation();
    });

    modal.addEventListener("click", () => {
      handleModalClose(modal);
    });
  });

  function handleModalClose(modal) {
    clearFormFields(modal);
    modal.classList.remove("active");

    modal.setAttribute("data-mode", "create");

    const submitBtn = document.getElementById("createVisitFormBntSubmit");
    const formTitle = document.querySelector(".create-visit__title");

    formTitle.textContent = "- Create visit -";
    submitBtn.textContent = "Create";
  }

  function clearFormFields(modal) {
    const form = modal.querySelector("form");
    form?.reset();

    const inputs = document.querySelectorAll('input');
    inputs.forEach(input => {
    input.classList.remove('error');
});

    const currentDoctorFields = modal.querySelector(
      ".create-visit__unique-fields"
    );
    if (currentDoctorFields) {
      addUniqueFieldsModal(doctorTypeSelect.value);
    }
  }
};

export default renderModals;