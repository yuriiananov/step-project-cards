import { cardsApi } from "../api/cards.js";
import getCardsAndRender from "./getCardsAndRender.js";

const addCard = async (e) => {
    e.preventDefault();

    const createCardForm = e.target;

    const data = new FormData(createCardForm);

    const formDataObject = {
        "goalvisit": "",
        "description": "",
        "doctor": "",
        "urgency": "",
        "clientname": "",
        "weight": "",
        "age": "",
        "bp": "",
        "diseases": "",
        "lastVisit": "",
        "status": "",
    }

    // Перетворення об'єкту FormData в об'єкт JavaScript
    data.forEach((value, key) => {
        formDataObject[key] = value;
    });

    // Добавляємо візит до бази
    await cardsApi.createCard(formDataObject);

    // Виводимо новий візит
    getCardsAndRender();

    // Очистка полів форми
    createCardForm.reset();

    // Ховаємо всі модальні вікна
    const modals = document.querySelectorAll('.modal');
    modals.forEach(modal => modal.classList.remove('active'))
}

export default addCard;