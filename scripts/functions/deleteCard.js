import { cardsApi } from "../api/cards.js";

const deleteCardFunc = async (id = false) => {
    try {
        if (id) {
            const res = await cardsApi.deleteCard(id);

            if (res.status == 200) {
                document.getElementById(`card-visit-${id}`).remove();
            }
        } else {
            throw ('Id не знайдено при видаленні візиту!')
        }
    } catch (err) {
        console.log(err.message);
    }
}

export default deleteCardFunc