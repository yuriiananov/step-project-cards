export default function validateForm() {
    const form = document.getElementById('createVisitForm');
    const inputs = form.querySelectorAll('input[required], select[required]');
    let isValid = true;

    inputs.forEach(input => {
        let value = input.value.trim();
        if (!value) {
            isValid = false;
            input.classList.add('error');
            alert('The field ' + input.name + ' cannot be empty.');
        } else {
            switch (input.id) {
                case 'clientIndex':
                case 'clientAge':
                    if (!Number.isInteger(+value) || +value <= 0) {
                        isValid = false;
                        input.classList.add('error');
                        alert('The field ' + input.name + ' must be a positive integer.');
                    } else {
                        input.classList.remove('error');
                    }
                    break;
                case 'clientPressure':
                    if (!Number.isInteger(+value) || +value < 60 || +value > 160) {
                        isValid = false;
                        input.classList.add('error');
                        alert('The field ' + input.name + ' must be an integer in the range of 60 to 160.');
                    } else {
                        input.classList.remove('error');
                    }
                    break;
                case 'clientName':
                    if (!value.split(' ')[1]) {
                        isValid = false;
                        input.classList.add('error');
                        alert('The field ' + input.name + ' must consist of two or more words.');
                    } else {
                        input.classList.remove('error');
                    }
                    break;
                default:
                    input.classList.remove('error');
            }
        }
    });

    return isValid;
}