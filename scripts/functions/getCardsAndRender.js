import { cardsApi } from "../api/cards.js";
import { toggleSlide } from "../helpers/index.js";
import { eventsCardVisit } from "./index.js";

const cardList = document.querySelector("#card-list");

const getCardsAndRender = async (arrayCards = false) => {
    try {

        if (arrayCards == false)
            arrayCards = await cardsApi.getCards();

        if (arrayCards && arrayCards.length) {

            cardList.innerHTML = '';

            arrayCards.forEach(({ id, goalvisit, description, doctor, urgency, clientname, weight, age, bp, diseases, lastVisit, status }) => {

                let boxMore = '';

                switch (doctor) {
                    case 'cardiologist':
                        boxMore = `<div class="card-visit-info-list">
                        <div class="card-visit-main-info">
                        <div>
                                    <span class="card-visit-title">${goalvisit}</span>
                                </div>
                                <div >
                                    <span class="card-description">${description}</span>
                                </div>
                        </div>
                                <div class="card-visit-info">
                                    <span>Blood preasure:</span>
                                    <span class="card-bp">${bp}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Age:</span>
                                    <span class="card-age">${age}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Weight:</span>
                                    <span class="card-weight">${weight}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Urgency:</span>
                                    <span class="card-urgency">${urgency}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Client:</span>
                                    <span class="card-clientName">${clientname}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Diseases:</span>
                                    <span class="card-diseases">${diseases}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Status:</span>
                                    <span class="card-status">${status}</span>
                                </div>
                            </div>`;
                        break;

                    case 'dentist':
                        boxMore = `<div class="card-visit-info-list">
                        <div class="card-visit-main-info">
                        <div>
                                    <span class="card-visit-title">${goalvisit}</span>
                                </div>
                                <div >
                                    <span class="card-description">${description}</span>
                                </div>
                        </div>
                                <div class="card-visit-info">
                                    <span>Status:</span>
                                    <span class="card-status">${status}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Urgency:</span>
                                    <span class="card-urgency">${urgency}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Client:</span>
                                    <span class="card-clientName">${clientname}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>LastVisit:</span>
                                    <span class="card-lastVisit">${lastVisit}</span>
                                </div>
                            </div>`;
                        break;

                    case 'therapist':
                        boxMore = `<div class="card-visit-info-list">
                        <div class="card-visit-main-info">
                        <div>
                                    <span class="card-visit-title">${goalvisit}</span>
                                </div>
                                <div >
                                    <span class="card-description">${description}</span>
                                </div>
                        </div>
                                
                                <div class="card-visit-info">
                                    <span>Status:</span>
                                    <span class="card-status">${status}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Urgency:</span>
                                    <span class="card-urgency">${urgency}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Client:</span>
                                    <span class="card-clientName">${clientname}</span>
                                </div>
                                <div class="card-visit-info">
                                    <span>Age:</span>
                                    <span class="card-age">${age}</span>
                                </div>
                            </div>`;
                        break;
                }

                cardList.insertAdjacentHTML(
                    'afterbegin',
                    `<li class="card-visit" id="card-visit-${id}">
                    <div class="card-visit-header">
                        <p class="card-visit-desc">${doctor}</p>
                        <div class="card-visit-action">
                            <a href="#" class="card-visit-btn btn-more" data-action="more">More</a>
                            <a href="#" data-card-id="${id}" class="card-visit-btn btn-edit" data-action="edit">Edit</a>
                            <a href="#" data-card-id="${id}" class="card-visit-btn btn-del" data-action="del">Delete</a>
                        </div>
                    </div>
                    <div class="card-visit-body">
                        <div class="card-visit-body-wrapper">
                            ${boxMore}
                        </div>
                    </div>
                </li>`)
            });



            cardList.querySelectorAll('li').forEach((el) => {

                const trigger = el.querySelector('[data-action="more"]');
                const target = el.querySelector('.card-visit-body')
                toggleSlide(target, trigger, 200)


                const btns = el.querySelector('.card-visit-action').querySelectorAll('a')
                btns.forEach((btn) => {
                    if (btn !== trigger) {
                        eventsCardVisit(btn)
                    }
                })


            })
        }
    } catch (error) {
        console.error(error.message);
    }
};

export default getCardsAndRender
