import { cardsApi } from "../api/cards.js";
import { getCardsAndRender } from "./index.js";

// Змінна для зберігання ідентифікатора таймера
let timeoutId;

// Фільтрування візитів
async function filterVisits(filterForm) {

    // Відбираємо всі дані з форми
    const formData = new FormData(filterForm);

    // Знаємо про недолік бази тому додатково фільтруємо масив візитів
    const fieldSearch = formData.get('search');
    const fieldStatus = formData.get('status');
    const fieldUrgency = formData.get('urgency');

    // Видалення попереднього таймера перед встановленням нового
    clearTimeout(timeoutId);

    // Встановлення нового таймера для затримки в 1 секунду перед викликом серверного запиту
    timeoutId = setTimeout(async () => {

        // Змінні для роботи фільтру
        let filteredArray = '';
        let arrayCards;

        // Попередньо витягуємо дані з бази 
        if (fieldSearch != '') {
            arrayCards = await cardsApi.getCards('?search=' + fieldSearch);
        } else {
            arrayCards = await cardsApi.getCards();
        }

        // Фільтрація масиву за значеннями з форми
        if (fieldStatus != '' && fieldUrgency != '') {

            // Перевірка, чи значення в об'єкті card відповідає значенням з форми
            filteredArray = arrayCards.filter(card => {
                return card.status == fieldStatus
                    && card.urgency == fieldUrgency;
            });

        } else {

            // Фільтрація масиву за значеннями з форми
            if (fieldStatus != '') {
                filteredArray = arrayCards.filter(card => {
                    return card.status == fieldStatus;
                });
            }

            // Фільтрація масиву за значеннями з форми
            if (fieldUrgency != '') {
                filteredArray = arrayCards.filter(card => {
                    return card.urgency == fieldUrgency;
                });
            }
        }

        // Якщо масив не сформований записуємо стандатрний вивід від сервера
        if (filteredArray === '') {
            filteredArray = arrayCards;
        }

        // Викликаємо функцію виводу візитів
        if (filteredArray.length != 0) {
            getCardsAndRender(filteredArray);
        } else {
            const cardList = document.querySelector("#card-list");
            cardList.innerHTML = '<li class="no-result">No visits have been find!</li>';
        }
    }, 600);

}

export default filterVisits;