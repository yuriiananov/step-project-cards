import axiosInstance from "./index.js";

const authorizationApi = {
    async login(body) {
        const { data } = await axiosInstance.post('/login', body)

        return data
    }
}

export {
    authorizationApi
}