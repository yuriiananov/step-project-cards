import axios from "../../node_modules/axios/dist/esm/axios.js";
import { AUTHORIZATION_TOKEN_KEY } from "../constants/index.js";

const axiosInstance = axios.create({
    baseURL: 'https://ajax.test-danit.com/api/v2/cards',
    headers: {
        'Content-Type': 'application/json',
    },
});

const axiosInstance2 = axios.create({
    baseURL: 'https://6558fecde93ca47020a9f921.mockapi.io/visits',
    headers: {
        'Content-Type': 'application/json',
    },
});

axiosInstance.interceptors.request.use(
    (config) => {

        const token = localStorage.getItem(AUTHORIZATION_TOKEN_KEY) || ''
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }

        return config;
    },
    (error) => {
        console.error('Request error:', error);
        return Promise.reject(error);
    }
);

export default axiosInstance
export { axiosInstance2 }