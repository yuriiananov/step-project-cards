import axiosInstance, { axiosInstance2 } from "./index.js";

const cardsApi = {
    async getCards(params = '') {
        const { data } = await axiosInstance2.get(params)
        return data
    },
    async getCardById(cardId) {
        const { data } = await axiosInstance2.get(`/${cardId}`)

        return data
    },
    async createCard(body) {
        const { data } = await axiosInstance2.post('', body)

        return data
    },
    async deleteCard(cardId) {
        const { data, status } = await axiosInstance2.delete(`/${cardId}`)

        return { data, status }
    },
    async updateCard(cardId, body) {
        const { data } = await axiosInstance2.put(`/${cardId}`, body)

        return data
    }
}

export {
    cardsApi
}