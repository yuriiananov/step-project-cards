import { AUTHORIZATION_TOKEN_KEY } from "./constants/index.js";
import { addCard, getCardsAndRender, loginFunc, renderModals, filterVisits } from './functions/index.js';
import { doctorTypeSelect, addUniqueFieldsModal } from "./functions/addUniqueFieldsModal.js";
import handleEditFormSubmit from "./functions/editFormSubmit.js";
import validateForm from './functions/validationForm.js';

const modalBtn = document.querySelector("#login-modal-button");



let IS_AUTHORIZED_USER = false;

const checkAuthorization = () => {

    if (IS_AUTHORIZED_USER) {

        const authBnt = document.getElementById('auth');
        const visitBnt = document.getElementById('create-visit');

        authBnt.classList.add('hide')
        visitBnt.classList.remove('hide')
    }

    console.log("LOADED");
}

modalBtn.addEventListener("click", (e) => {
    e.preventDefault();
    loginFunc((auth) => {
        IS_AUTHORIZED_USER = auth.authorized;
        getCardsAndRender()
        checkAuthorization()
        document.querySelectorAll('.modal').forEach((m) => m.classList.remove('active'))
    }
    )
});

if (localStorage.getItem(AUTHORIZATION_TOKEN_KEY)) {
    IS_AUTHORIZED_USER = true;
    getCardsAndRender();
    checkAuthorization();
}




renderModals()

doctorTypeSelect.addEventListener("change", () => {
    const selectedDoctor = doctorTypeSelect.value;

    addUniqueFieldsModal(selectedDoctor);
});




/**
 * Оголошуємо функціонал добавлення візиту
 */

const createCardForm = document.getElementById('createVisitForm');
createCardForm.onsubmit = (e) => {
    e.preventDefault();

     // Перевіряємо, чи форма проходить валідацію
     if (!validateForm()) {
        return;
    }

    const editCardId = document.getElementById('editCardId').value;

    if (editCardId) {
        handleEditFormSubmit(e);
    } else {
        addCard(e);
    }
};



/**
 * Оголошуємо функціонал фільтрації візиту
 */
const filterForm = document.querySelector('.cards-filter__form');

filterForm.addEventListener('input', () => {

    filterVisits(filterForm);
});
