export class Modal {

    constructor(children) {
        this.children = children;
        this.isOpen = false;
    }

    render() {
    }

    open() {
        this.isOpen = true;
        this.render();
    }

    close() {
        this.isOpen = false;
        this.render();
    }
}