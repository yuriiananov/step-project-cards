import { Visit } from "./Visit.js";

export class VisitCardiologist extends Visit {
    constructor({ doctor, title, clientName, description, urgency, status, diseases, age, bp, weight }) {
        super({ doctor, title, clientName, description, urgency, status });
        this.bp = bp;
        this.weight = weight;
        this.diseases = diseases;
        this.age = age;
    }

    getVisitData() {
        return {
            ...this.getCommonVisitData(),
            bp: this.bp,
            weight: this.weight,
            diseases: this.diseases,
            age: this.age
        }
    }


    createVisitToDoctor() {
        this.createVisit(this.getVisitData())
    }

}